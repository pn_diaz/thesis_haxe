-- @path haxe=/thesis_haxe/model/haxe.ecore
-- @atlcompiler emftvm

module haxeJavaRefactoring; 
create OUT: haxe refining IN: haxe;

helper context haxe!HaxeMethodInvocation def: isASystemOutPrintlnCall(): Boolean =
	if (self.expression.oclIsUndefined()) then
		false
	else
		if (self.method.oclIsUndefined()) then
			false
		else
			self.method.isprintln() and self.expression.isSystemOutAccess()
		endif
	endif;

helper context haxe!HaxeMethodInvocation def: isAMathHypotCall(): Boolean =
	if (self.expression.oclIsUndefined()) then
		false
	else
		if (self.method.oclIsUndefined()) then
			false
		else
			self.method.isHypot() -- and self.expression.isMathHypotAccess()
			
		endif
	endif;

helper context haxe!HaxeMethodInvocation def: isASystemOutPrintCall(): Boolean =
	if (self.expression.oclIsUndefined()) then
		false
	else
		if (self.method.oclIsUndefined()) then
			false
		else
			self.method.isprint() and self.expression.isSystemOutAccess()
		endif
	endif;

helper context haxe!HaxeExpression def: isSystemOutAccess(): Boolean =
	false;

helper context haxe!HaxeSingleVariableAccess def: isSystemOutAccess(): Boolean =
	if (self.qualifier.oclIsUndefined()) then
		false
	else
		self.variable.isout() and self.qualifier.isSystemAccess()
	endif;

helper context haxe!HaxeClassifierAccess def: isSystemAccess(): Boolean =
	self.referencedTerminal.isSystem() and self.parameterMapping.isEmpty();

helper context haxe!HaxeAbstractOperation def: isprintln(): Boolean =
	false;

helper context haxe!HaxeAbstractOperation def: isprint(): Boolean =
	false;

helper context haxe!HaxeOperation def: isprintln(): Boolean =
	self.name = 'println';

helper context haxe!HaxeOperation def: isprint(): Boolean =
	self.name = 'print';

helper context haxe!HaxeVariableDeclaration def: isout(): Boolean =
	self.name = 'out';

helper context haxe!HaxeType def: isSystem(): Boolean =
	self.name = 'System';

helper context haxe!HaxePackage def: isJava(): Boolean =
	self.name = 'java' and self.parentReference -> oclIsUndefined();

helper context haxe!HaxePackage def: isJavaLang(): Boolean =
	if (self.parentReference -> oclIsUndefined()) then
		false
	else
		self.name = 'lang' and self.parentReference -> isJava()
	endif;

helper context haxe!HaxeClass def: isJavaLangMath(): Boolean =
	self.name = 'Math' and self.parentReference -> isJavaLang();

helper context haxe!HaxePathReferentiable def: isJavaLangString(): Boolean =
	false;
helper context haxe!HaxeClass def: isJavaLangString(): Boolean =
	if (self.parentReference.oclIsUndefined()) then false else self.name = 'String' and self.parentReference -> isJavaLang() endif;

helper context haxe!HaxeOperation def : isJavaLangStringEquals() : Boolean = 
	self.name='equals' and self.fieldContainer->isJavaLangString();

helper context haxe!HaxeAbstractOperation def : isJavaLangStringEquals() : Boolean = 
	false;

helper context haxe!HaxeExpression def : isStringExp() : Boolean =
	false;

helper context haxe!HaxeSingleVariableAccess def : isStringExp() : Boolean =
	self.variable->isStringTyped();

helper context haxe!HaxeStringLiteral def : isStringExp() : Boolean = true;

helper context haxe!HaxeVariableDeclaration def : isStringTyped() :Boolean= 
	false;

helper context haxe!HaxeSingleVariableDeclaration def : isStringTyped() :Boolean= 
	if (self.type.oclIsUndefined()) then false else self.type->accessJavaLangString() endif;

helper context haxe!HaxeVariableDeclarationFragment def : isStringTyped() :Boolean= 
	if (self.variablesContainer.type.oclIsUndefined()) then false else self.variablesContainer.type->accessJavaLangString() endif;

helper context haxe!HaxeVariableDeclarationExpression def : isStringTyped() :Boolean= 
	self.groups->select(z|z.type.referencedTerminal->isJavaLangString())->notEmpty();

helper context haxe!HaxeOperation def: isHypot(): Boolean =
	self.fieldContainer -> isJavaLangMath() and self.name = 'Hypot';

helper context haxe!HaxeTypeAccess def: accessJavaLangString(): Boolean =
	false;

helper context haxe!HaxeClassifierAccess def: accessJavaLangString(): Boolean =
	if ( self.referencedTerminal .oclIsUndefined()) 
	then false else self.referencedTerminal  -> isJavaLangString() endif;

helper context haxe!HaxeFunctionTypeAccess def: accessJavaLangString(): Boolean =
	self.argumentTypes -> exists(x | x -> accessJavaLangString()) or self.returnType ->
			accessJavaLangString();


helper context haxe!HaxeTypeAccess def: accessJavaLang(): Boolean =
	false;

helper context haxe!HaxeClassifierAccess def: accessJavaLang(): Boolean =
	let x : haxe!HaxePathReferentiable = self.referencedTerminal in 
	if (not x.oclIsUndefined()) then
	let w : haxe!HaxePackage = x.parentReference in
	if ( not w.oclIsUndefined()) then w -> isJavaLang() else false endif else false endif;

helper context haxe!HaxeFunctionTypeAccess def: accessJavaLang(): Boolean =
	self.argumentTypes -> exists(x | x -> accessJavaLang()) or self.returnType ->
			accessJavaLang();


helper context haxe!HaxeModelElement def: getParentModule(): haxe!HaxeModule =
	let c : haxe!HaxeModelElement = self.refImmediateComposite() in
	if (c.oclIsKindOf(haxe!HaxeType)) then c.containerModule else 
	if c.oclIsUndefined() then OclUndefined else c-> getParentModule() endif endif;

helper context haxe!HaxeModule def : hasJavaLangTypeAccess() : Boolean=
haxe!HaxeTypeAccess.allInstances()->select(x|x.getParentModule()=self)->select(x|x.accessJavaLang())->notEmpty();
	
helper context haxe!HaxeModule def: calcDeps() : Sequence(haxe!HaxeImportDeclaration) =
haxe!HaxeClassifierAccess.allInstances()->select(x|x.getParentModule()=self and x.accessJavaLang())->collect(z|z.referencedTerminal)->asSet()->reject(e|e.isJavaLangString())->collect(w|haxe!HaxeImportDeclaration->newInstance()->refSetValue('referencedTerminal' , w));
			
abstract rule methodReplacement  {
	from
		s: haxe!HaxeMethodInvocation 
	to
		t: haxe!HaxeMethodInvocation (
			arguments <- s.arguments,
			comments <- s.comments
		)
}

--	rule hypot
--	{
--		from
--			s: haxe!HaxeMethodInvocation
--			(s.isAMathHypotCall().debug())
--
--	}


rule javaxpackage {
	from
		s: haxe!HaxePackage (
			s.name = 'javax' and s.parentReference.
					oclIsUndefined()
		)
	to
		t: haxe!HaxePackage
		(
			name <- s.name,
			comments <- s.comments,
			childrenReferences <- s.childrenReferences,
			parentReference <- s.refImmediateComposite().elements->select(x|x.name='java')->first()
		)
}

-- this rule deletes the weird "default package" extracted from MoDisco ... 
--rule weirdpackage {
--	from
--		s: haxe!HaxePackage (
--			s.name = '(default package)' and s.parentReference.oclIsUndefined()
--		)
--	to
--	drop
--}

rule stringEquals 
{
	from
	s : haxe!HaxeMethodInvocation(
								    if (s.expression.oclIsUndefined()) then false else s.expression.isStringExp() endif 
									and s.method.isJavaLangStringEquals()
									and s.arguments->size() = 1
									and s.arguments->reject(x|x.isStringExp())->isEmpty())
		
	to 
	t : haxe!HaxeInfixExpression
		( 
			leftSide <- s.expression,
			operator <- #EQ,
			rigthSide <- s.arguments->first()
		)
	
}

rule sysoutprintln extends methodReplacement {
	from
		s: haxe!HaxeMethodInvocation (
			s.isASystemOutPrintlnCall()
		)
	to
		t: haxe!HaxeMethodInvocation (
			expression <- n,
			method <- haxe!HaxeOperation.allInstances() -> select(x | x.isprintln()) ->
					first()
		),
		n: haxe!HaxeClassifierAccess (
			referencedTerminal <- haxe!HaxeClass.allInstances() -> select(x | x.name =
					'Sys').first()
		)
}

rule sysoutprint extends methodReplacement{
	from
		s: haxe!HaxeMethodInvocation (
			s.isASystemOutPrintCall().debug()
		)
	to
		t: haxe!HaxeMethodInvocation (
			expression <- n,
			method <- haxe!HaxeOperation.allInstances() -> select(x | x.isprint()) ->
					first()
		),
		n: haxe!HaxeClassifierAccess (
			referencedTerminal <- haxe!HaxeClass.allInstances() -> select(x | x.name =
					'Sys').first()
		)
}
