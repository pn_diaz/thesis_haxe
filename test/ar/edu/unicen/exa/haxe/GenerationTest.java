/**
 * 
 */
package ar.edu.unicen.exa.haxe;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.BasicMonitor.Printing;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IExtractor;
import org.eclipse.m2m.atl.core.IInjector;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.core.IReferenceModel;
import org.eclipse.m2m.atl.core.emf.EMFExtractor;
import org.eclipse.m2m.atl.core.emf.EMFInjector;
import org.eclipse.m2m.atl.core.emf.EMFModel;
import org.eclipse.m2m.atl.core.emf.EMFModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFReferenceModel;
import org.eclipse.m2m.atl.core.launch.ILauncher;
import org.eclipse.m2m.atl.engine.emfvm.launch.EMFVMLauncher;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.io.Files;

/**
 * @author Pablo Nicolas Diaz
 * 
 */
public class GenerationTest {

	private final Main m;
	private final ILauncher transformationLauncher;
	private final IExtractor extractor;
	private final IInjector injector;
	private final EMFModelFactory modelFactory;
	private final EMFReferenceModel haxeMetamodel;
	private final EMFReferenceModel javaMetamodel;
	private final Printing printing;
	private final IReferenceModel refiningTraceMetamodel;

	public GenerationTest() throws ATLCoreException {
		transformationLauncher = new EMFVMLauncher();
		modelFactory = new EMFModelFactory();
		injector = new EMFInjector();
		extractor = new EMFExtractor();

		refiningTraceMetamodel = modelFactory
				.getBuiltInResource("RefiningTrace.ecore"); //$NON-NLS-1$

		haxeMetamodel = (EMFReferenceModel) modelFactory.newReferenceModel();
		injector.inject(haxeMetamodel, "model/haxe.ecore");
		javaMetamodel = (EMFReferenceModel) modelFactory.newReferenceModel();
		injector.inject(javaMetamodel, "model/java.ecore");
		printing = new Printing(System.out);
		m = new Main();

	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testHelloWorld() throws ATLCoreException, IOException,
			InterruptedException {
		test("helloWorld", "Hello World");
	}

	@Test
	public void testEmpty() throws ATLCoreException, IOException,
			InterruptedException {
		test("empty", "");

	}

	@Test
	// @Ignore
	public void testCCtalk() throws ATLCoreException, IOException,
			InterruptedException {
		transformate("cctalk");// , "");
		generate("cctalk");
	}

	@Test
	public void testClasses() throws ATLCoreException, IOException,
			InterruptedException {
		test("classes", "OtherClass", "Creating SimpleClass",
				"Creating InnerClasses", "Making PrivateInnerClass",
				"Creating InnerClasses.PrivateInnerClass",
				"Calling InnerClasses Object", "I'm InnerClasses",
				"Calling InnerClasses.PrivateInnerClass Object",
				"I'm The Inner", "Creating MultipleConstructors with 0 args",
				"Creating MultipleConstructors with 1 as arg");
	}

	@Test
	public void testAnnotation() throws ATLCoreException, IOException {
		transformate("annotation");
	}

	@Test
	public void testMandelrotRefactorized() throws ATLCoreException,
			IOException {
		transformate("mandelbrot_refactorized");
		generate("mandelbrot_refactorized");
	}

	@Test
	public void testJoda() throws ATLCoreException, IOException {
		transformate("joda-time");
		generate("joda-time");
	}

	@Test
	public void testLiterals() throws ATLCoreException, IOException {
		transformate("literals");
	}

	@Test
	public void testExpressions() throws ATLCoreException, IOException {
		transformate("expressions");
	}

	@Test
	public void testInheritance() throws ATLCoreException, IOException,
			InterruptedException {
		test("inheritance");
	}

	@Test
	public void testEnums() throws ATLCoreException, IOException,
			InterruptedException {
		test("enums");
	}

	@Test
	public void testStatements() throws ATLCoreException, IOException,
			InterruptedException {
		String stat = "statements";
		test(stat, "test", "test", "test", "test", "test", "test", "test",
				"test", "test", "test");
	}

	private void transformate(String modelName) throws ATLCoreException,
			IOException {

		IModel theJavaModel = modelFactory.newModel(javaMetamodel);

		injector.inject(theJavaModel, "test_models/in/" + modelName
				+ "_java.xmi");

		EMFModel theHaxeModel = modelFactory.newModel(haxeMetamodel,
				"haxe.ecore");

		FileInputStream f = new FileInputStream("model/java2haxe.asm");
		transformationLauncher.initialize(new HashMap<String, Object>());
		transformationLauncher.addInModel(theJavaModel, "IN", "java");
		transformationLauncher.addOutModel(theHaxeModel, "OUT", "haxe");
		launchTransformation(transformationLauncher, f);

		FileInputStream w = new FileInputStream("model/haxeImports.asm");

		IModel refiningTraceModel = modelFactory
				.newModel(refiningTraceMetamodel);
		transformationLauncher.initialize(new HashMap<String, Object>());
		transformationLauncher.addOutModel(refiningTraceModel,
				"refiningTrace", "RefiningTrace"); //$NON-NLS-1$ //$NON-NLS-2$
		transformationLauncher.addInOutModel(theHaxeModel, "IN", "haxe");
		launchTransformation(transformationLauncher, w);

		String outModelName = "test_models/out_models/" + modelName
				+ "_haxe.xmi";

		Map<String, Object> extractOptions = new HashMap<String, Object>();
		extractOptions.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
		extractor.extract(theHaxeModel, outModelName, extractOptions);

	}

	private void launchTransformation(ILauncher l,
			FileInputStream transformationFile) throws FileNotFoundException {
		l.launch(ILauncher.RUN_MODE, BasicMonitor.toIProgressMonitor(printing),
				new HashMap<String, Object>(), transformationFile);
	}

	private void generate(String modelName) throws IOException {
		String outModelName = "test_models/out_models/" + modelName
				+ "_haxe.xmi";

		m.initialize(URI.createFileURI(outModelName), new File(
				"test_models/out_text/" + modelName + "_haxe"),
				new ArrayList<Object>());

		m.doGenerate(printing);

	}

	private boolean compile(String prefix) throws IOException,
			InterruptedException {
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(new String[] { "haxe", "build.hxml" }, null,
				new File("test_models/out_text/" + prefix + "_haxe"));
		int exitVal = proc.waitFor();
		// System.out.println("Process exitValue: " + exitVal);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				proc.getErrorStream()));
		String line = reader.readLine();
		while (line != null) {
			System.out.println(line);
			line = reader.readLine();
		}
		return exitVal == 0;
	}

	private boolean run(String prefix, String... expectedOutput)
			throws IOException, InterruptedException {
		Files.move(new File("test_models/out_text/" + prefix + "_haxe/out"),
				new File("test_models/out_text/" + prefix + "_haxe/out.n"));
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec("neko out.n", null, new File(
				"test_models/out_text/" + prefix + "_haxe"));
		int exitVal = proc.waitFor();
		// System.out.println("Process exitValue: " + exitVal);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				proc.getInputStream()));
		Iterator<String> tokenizer = Arrays.asList(expectedOutput).iterator();
		String line = reader.readLine();
		String lineExpected = tokenizer.hasNext() ? tokenizer.next() : "";
		while (line != null && lineExpected != null) {
			System.out.println(line);
			System.out.println(lineExpected);
			if (!line.equals(lineExpected))
				return false;
			line = reader.readLine();
			lineExpected = tokenizer.hasNext() ? tokenizer.next() : null;
		}
		return exitVal == 0 && !tokenizer.hasNext();
	}

	private void test(String tag, String... out) throws ATLCoreException,
			IOException, InterruptedException {
		transformate(tag);
		generate(tag);
		assertTrue(compile(tag));
		assertTrue(run(tag, out));
	}
}
